@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>User Report</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>First Name </th>
                                <th>Last Name </th>
                                <th>Report Type</th>
                                <th>Report Description</th>
                                <th>From Worker ID</th>
                                
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($user_report_info as $v_user_report)
                        @if ($v_user_report->action == 0)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_user_report->id }}</td>
                                        <td>{{ $v_user_report->first_name }}</td>
                                        <td>{{ $v_user_report->last_name }}</td>
                                        <td>{{ $v_user_report->report_type }}</td>
                                        <td>{{ $v_user_report->report_description }}</td>
                                        <td>{{ $v_user_report->worker_id }}</td>
                                        

                                        <td class="text-center">
                                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                            <button class="btn btn-danger" type="button">Suspend</button>
                                            <div class="btn-group" role="group">
                                                 <button class="btn btn-danger dropdown-toggle" id="btnGroupDrop4" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                             <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{ URL::to('/user-report-suspend3/'.$v_user_report->user_id) }}">3 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/user-report-suspend5/'.$v_user_report->user_id) }}">5 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/user-report-suspend8/'.$v_user_report->user_id) }}">8 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/user-report-suspend10/'.$v_user_report->user_id) }}">10 Days</a>
                                             </div>
                                          </div>
                                         </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $user_report_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

