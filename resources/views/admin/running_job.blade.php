@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Running Jobs</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>User First Name </th>
                                <th>Category Name </th>
                                <th>Job Title</th>
                                <th>Description</th>
                                <th>Address</th>
                                <th>Area</th>
                                <th>Job Time</th>
                                
                            </tr>
                        </thead>
                        @foreach ($running_job_info as $v_running_job)
                        @if ($v_running_job->is_done == 0 && $v_running_job->in_progress ==1 && $v_running_job->is_canceled == 0)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_running_job->job_id }}</td>
                                        <td>{{ $v_running_job->first_name }}</td>
                                        <td>{{ $v_running_job->category_name }}</td>
                                        <td>{{ $v_running_job->job_title }}</td>
                                        <td>{{ $v_running_job->job_description }}</td>
                                        <td>{{ $v_running_job->job_address }}</td>
                                        <td>{{ $v_running_job->area_name }}</td>
                                        <td>{{ $v_running_job->job_time }}</td>
                                       
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $running_job_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

