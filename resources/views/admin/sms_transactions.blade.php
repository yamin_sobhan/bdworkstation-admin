@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>SMS Transactions</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Worker Name </th>
                                <th>Transaction Type </th>
                                <th>Package Name</th>
                                <th>Package Price</th>
                                <th>Payment Method</th>
                                <th>Transaction Time</th>
                                 <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        
                        @foreach ($sms_info as $v_sms)
                        
                        @if ($v_sms->is_approved == 1 && $v_sms->transaction_type == 'SMS_CREDIT')
                        <tbody>
                                    <tr>
                                        <td>{{ $v_sms->worker_id }}</td>
                                        <td>{{ $v_sms->first_name }} {{ $v_sms->last_name }}</td>
                                        <td>{{ $v_sms->transaction_type }}</td>
                                        <td>{{ $v_sms->package_name }}</td>
                                        <td>{{ $v_sms->package_price }}</td>
                                        <td>{{ $v_sms->payment_method }}</td>
                                        <td>{{ $v_sms->transaction_time }}</td>
                                        
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/sms_pdf/'.$v_sms->transaction_id) }}" class="btn btn-sm btn-success" id="verified">Generate PDF</a>
                                            </div>
                                        </td>
                                        
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                        

                        
                    </table>
                    {{ $sms_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

