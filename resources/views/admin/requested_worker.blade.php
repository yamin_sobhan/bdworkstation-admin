@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Requested Worker</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>First Name </th>
                                <th>Last Name </th>
                                <th>Phone Number</th>
                                <th>Address</th>
                                <th>Area</th>
                                <th>Category</th>
                                <!-- <th>SubCategory</th> -->
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($requested_worker_info as $v_requested_worker)
                        @if ($v_requested_worker->is_verified == 0)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_requested_worker->worker_id }}</td>
                                        <td>{{ $v_requested_worker->first_name }}</td>
                                        <td>{{ $v_requested_worker->last_name }}</td>
                                        <td>{{ $v_requested_worker->phone_number }}</td>
                                        <td>{{ $v_requested_worker->address }}</td>
                                        <td>{{ $v_requested_worker->area_name }}</td>
                                        <td>{{ $v_requested_worker->category_name }}</td>
                                        <td>{{ $v_requested_worker->file_type }}</td>
                                        <td><a href="{{ URL::to('images/worker/'.$v_requested_worker->verification_file_front) }}" target="_blank">Front Image</a></td>
                                        <td><a href="{{ URL::to('images/worker/'.$v_requested_worker->verification_file_back) }}" target="_blank">Back Image</a></td>
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/verified_worker/'.$v_requested_worker->worker_id) }}" class="btn btn-sm btn-success" id="verified"><i class="fa fa-check-circle">Verify</i></a>
                                            </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $requested_worker_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

