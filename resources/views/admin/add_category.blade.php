@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Add Category</h1>
        </div>
</div>

    <p class="alert-success">

            <?php
                $message = Session::get('message');
                
                if ($message) {
                    echo $message;
                    Session::put('message', NULL);
                }
            ?>

        </p>

    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <form action="{{ url('/save-category')}}" method="POST" role="form" enctype="multipart/form-data">
                    @csrf
                    <div class="tile-body">
                        <div class="form-group">
                            <label class="control-label" for="name">Category Name <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control" type="text" name="category_name" required>
                        </div>   
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add Category</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection