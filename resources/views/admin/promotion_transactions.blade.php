@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Promotion Transactions</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Worker Name </th>
                                <th>Transaction Type </th>
                                <th>Package Name</th>
                                <th>Package Price</th>
                                <th>Payment Method</th>
                                <th>Transaction Time</th>
                                 <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        
                        @foreach ($promotion_info as $v_promotion)
                        
                        @if ($v_promotion->is_approved == 1 && $v_promotion->transaction_type == 'PROMOTION_CREDIT')
                        <tbody>
                                    <tr>
                                        <td>{{ $v_promotion->worker_id }}</td>
                                        <td>{{ $v_promotion->first_name }} {{ $v_promotion->last_name }}</td>
                                        <td>{{ $v_promotion->transaction_type }}</td>
                                        <td>{{ $v_promotion->package_name }}</td>
                                        <td>{{ $v_promotion->package_price }}</td>
                                        <td>{{ $v_promotion->payment_method }}</td>
                                        <td>{{ $v_promotion->transaction_time }}</td>
                                        
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/promotion_pdf/'.$v_promotion->transaction_id) }}" class="btn btn-sm btn-success" id="verified">Generate PDF</a>
                                            </div>
                                        </td>
                                        
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                        

                        
                    </table>
                    {{ $promotion_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

