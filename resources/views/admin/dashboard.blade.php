@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
        </div>
    </div>
    <div class="row">
            <div class="col-md-6 col-lg-3">
            <a href="{{ URL::to('/requested_user') }}">
                <div class="widget-small primary coloured-icon">
                    <i class="icon fa fa-users fa-3x"></i>
                    <div class="info">
                        <h4>Requested Users</h4>
                        <p><b>{{ $count_requested_user }}</b></p>
                    </div>
                </div>
                </a>
            </div>
        
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/active_user') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Active Users</h4>
                    <p><b>{{ $count_active_user }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/deleted_user') }}">
            <div class="widget-small primary coloured-icon">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Deleted Users</h4>
                    <p><b>{{ $count_deleted_user }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/suspended_user') }}">
            <div class="widget-small danger">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Suspended Users</h4>
                    <p><b>{{ $count_suspended_user }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/requested_worker') }}">
            <div class="widget-small primary coloured-icon">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Requested Workers</h4>
                    <p><b>{{ $count_requested_worker }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/active_worker') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Active Workers</h4>
                    <p><b>{{ $count_active_worker }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/deleted_worker') }}">
            <div class="widget-small primary coloured-icon">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Deleted WOrkers</h4>
                    <p><b>{{ $count_deleted_worker }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/suspended_worker') }}">
            <div class="widget-small danger">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Suspended Worker</h4>
                    <p><b>{{ $count_suspended_worker }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/complete_job') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Completed Job</h4>
                    <p><b>{{ $count_completed_job }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/running_job') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Running Jobs</h4>
                    <p><b>{{ $count_running_job }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/open_job') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Open Jobs</h4>
                    <p><b>{{ $count_open_job }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/sms_transactions') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>SMS Transactions</h4>
                    <p><b>{{ $count_sms_transactions }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/promotion_transactions') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Promotion Transactions</h4>
                    <p><b>{{ $count_promotion_transactions }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/all_sms_package') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>SMS Packages</h4>
                    <p><b>{{ $count_sms_packages }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/all_promotion_package') }}">
            <div class="widget-small primary">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Promotion Packages</h4>
                    <p><b>{{ $count_promotion_packages }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/user_report') }}">
            <div class="widget-small danger">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>User Report</h4>
                    <p><b>{{ $count_user_report }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/worker_report') }}">
            <div class="widget-small danger">
                <i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Worker Report</h4>
                    <p><b>{{ $count_worker_report }}</b></p>
                </div>
            </div>
            </a>
        </div>
        
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/all-category') }}">
            <div class="widget-small warning coloured-icon">
                <i class="icon fa fa-files-o fa-3x"></i>
                <div class="info">
                    <h4>Categories</h4>
                    <p><b>{{ $count }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/all-sub-category') }}">
            <div class="widget-small warning coloured-icon">
                <i class="icon fa fa-files-o fa-3x"></i>
                <div class="info">
                    <h4>Sub Categories</h4>
                    <p><b>{{ $countSub }}</b></p>
                </div>
            </div>
            </a>
        </div>
        
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/all-area') }}">
            <div class="widget-small warning coloured-icon">
                <i class="icon fa fa-files-o fa-3x"></i>
                <div class="info">
                    <h4>Area</h4>
                    <p><b>{{ $countArea }}</b></p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-6 col-lg-3">
        <a href="{{ URL::to('/all-service') }}">
            <div class="widget-small warning coloured-icon">
                <i class="icon fa fa-files-o fa-3x"></i>
                <div class="info">
                    <h4>Service</h4>
                    <p><b>{{ $countService }}</b></p>
                </div>
            </div>
            </a>
        </div>
    </div>
@endsection