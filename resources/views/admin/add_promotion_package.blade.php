@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Add Promotion Package</h1>
        </div>
</div>

    <p class="alert-success">

            <?php
                $message = Session::get('message');
                
                if ($message) {
                    echo $message;
                    Session::put('message', NULL);
                }
            ?>

        </p>
    </div>

    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <form action="{{ url('/save_promotion_package') }}" method="POST" role="form" enctype="multipart/form-data">
                    @csrf
                        <div class="tile-body">
                            <div class="form-group">
                                <label for="parent">Package Name<span class="m-l-5 text-danger"> *</label>
                                <input class="form-control" type="text" name="package_name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="name">Package Price <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control" type="number" name="package_price" required>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="name">Package Validation <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control" type="number" name="package_validation_in_days" required>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Category</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection