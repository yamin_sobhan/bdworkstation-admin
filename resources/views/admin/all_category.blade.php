@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>All Category</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Name </th>
                                
                            </tr>
                        </thead>
                        @foreach ($all_category_info as $v_category)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_category->category_id }}</td>
                                        <td>{{ $v_category->category_name }}</td>

                                        
                                    </tr>
                            
                        </tbody>
                        @endforeach
                    </table>
                    {{ $all_category_info->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

