@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Worker Report</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>First Name </th>
                                <th>Last Name </th>
                                <th>Report Type</th>
                                <th>Report Description</th>
                                <th>From User ID</th>
                                
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($worker_report_info as $v_worker_report)
                        @if ($v_worker_report->action == 0)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_worker_report->id }}</td>
                                        <td>{{ $v_worker_report->first_name }}</td>
                                        <td>{{ $v_worker_report->last_name }}</td>
                                        <td>{{ $v_worker_report->report_type }}</td>
                                        <td>{{ $v_worker_report->report_description }}</td>
                                        <td>{{ $v_worker_report->user_id }}</td>
                                        

                                        <td class="text-center">
                                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                            <button class="btn btn-danger" type="button">Suspend</button>
                                            <div class="btn-group" role="group">
                                                 <button class="btn btn-danger dropdown-toggle" id="btnGroupDrop4" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                             <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{ URL::to('/worker-report-suspend3/'.$v_worker_report->worker_id) }}">3 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/worker-report-suspend5/'.$v_worker_report->worker_id) }}">5 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/worker-report-suspend8/'.$v_worker_report->worker_id) }}">8 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/worker-report-suspend10/'.$v_worker_report->worker_id) }}">10 Days</a>
                                             </div>
                                          </div>
                                         </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $worker_report_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

