@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Questions</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Job ID</th>
                                <th>Question</th>
                                
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($question_info as $v_question)
                        @if ($v_question->is_active == 1)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_question->id }}</td>
                                        <td>{{ $v_question->job_id }}</td>
                                        <td>{{ $v_question->question }}</td>
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/deactivate_question/'.$v_question->id) }}" class="btn btn-sm btn-success" id="verified"><i class="fa fa-check-circle"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $question_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

