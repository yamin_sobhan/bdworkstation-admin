@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Promotion Packages</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Package Name</th>
                                <th>Package Price</th>
                                <th>Package Validation</th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($all_promotion_package as $v_promotion_package)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_promotion_package->id}}</td>
                                        <td>{{ $v_promotion_package->package_name }}</td>
                                        <td>{{ $v_promotion_package->package_price }}</td>
                                        <td>{{ $v_promotion_package->package_validation_in_days }}</td>
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/delete_promotion_package/'.$v_promotion_package->id) }}" class="btn btn-sm btn-danger" id="verified">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endforeach
                    </table>
                    {{ $all_promotion_package->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

