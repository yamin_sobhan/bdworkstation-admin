@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>All Service</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Parent Sub Category </th>
                                <th>Service Name</th>
                                
                            </tr>
                        </thead>
                        @foreach ($all_service_info as $v_service)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_service->service_id}}</td>
                                        <td>{{ $v_service->sub_category_name }}</td>
                                        <td>{{ $v_service->service_name }}</td>

                                        
                                    </tr>
                            
                        </tbody>
                        @endforeach
                    </table>
                    {{ $all_service_info->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

