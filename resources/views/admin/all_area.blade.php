@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>All Area</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Name </th>
                                
                            </tr>
                        </thead>
                        @foreach ($all_area_info as $v_area)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_area->area_id }}</td>
                                        <td>{{ $v_area->area_name }}</td>

                                        
                                    </tr>
                            
                        </tbody>
                        @endforeach
                    </table>
                    {{ $all_area_info->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

