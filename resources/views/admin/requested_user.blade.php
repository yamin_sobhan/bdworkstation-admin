@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Requested User</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>User Name </th>
                                
                                <th>Phone Number</th>
                                <th>Address</th>
                                <th>Area</th>
                                <th>File_type</th>
                                <th>Front</th>
                                <th>Back</th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($requested_user_info as $v_requested_user)
                        @if ($v_requested_user->is_verified == 0)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_requested_user->user_id }}</td>
                                        <td>{{ $v_requested_user->first_name }} {{ $v_requested_user->last_name }}</td>
                                        
                                        <td>{{ $v_requested_user->phone_number }}</td>
                                        <td>{{ $v_requested_user->address }}</td>
                                        <td>{{ $v_requested_user->area_name }}</td>
                                        <td>{{ $v_requested_user->file_type }}</td>
                                     
                                        <td><a href="{{ URL::to('images/user/'.$v_requested_user->verification_file_front) }}" target="_blank">Front Image</a></td>
                                        <td><a href="{{ URL::to('images/user/'.$v_requested_user->verification_file_back) }}" target="_blank">Back Image</a></td>
                                        		
                                        
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/verified_user/'.$v_requested_user->user_id) }}" class="btn btn-sm btn-success" id="verified"><i class="fa fa-check-circle">Verify</i></a>
                                            </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $requested_user_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

