@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>New Worker</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>User Name </th>
                                <th>Phone Number</th>
                                <th>Address</th>
                                <th>Area</th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($new_worker_info as $v_new_worker)
                        @if ($v_new_worker->is_verified == 0 && $v_new_worker->is_activated == 0 && $v_new_worker->is_deleted == 0)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_new_worker->worker_id }}</td>
                                        <td>{{ $v_new_worker->first_name }} {{ $v_new_worker->last_name }}</td>
                                        
                                        <td>{{ $v_new_worker->phone_number }}</td>
                                        <td>{{ $v_new_worker->address }}</td>
                                        <td>{{ $v_new_worker->area_name }}</td>
                                        
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/delete_new_worker/'.$v_new_worker->worker_id) }}" class="btn btn-sm btn-danger" id="verified">Delete</a>
                                            </div>
                                        </td>

                                        <td class="text-center">
                                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                            <button class="btn btn-danger" type="button">Suspend</button>
                                            <div class="btn-group" role="group">
                                                 <button class="btn btn-danger dropdown-toggle" id="btnGroupDrop4" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                             <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{ URL::to('/new-worker-suspend3/'.$v_new_worker->worker_id) }}">3 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/new-worker-suspend5/'.$v_new_worker->worker_id) }}">5 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/new-worker-suspend8/'.$v_new_worker->worker_id) }}">8 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/new-worker-suspend10/'.$v_new_worker->worker_id) }}">10 Days</a>
                                             </div>
                                          </div>
                                         </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $new_worker_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

