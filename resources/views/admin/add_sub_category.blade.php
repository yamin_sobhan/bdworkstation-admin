@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Add Sub Category</h1>
        </div>
</div>

    <p class="alert-success">

            <?php
                $message = Session::get('message');
                
                if ($message) {
                    echo $message;
                    Session::put('message', NULL);
                }
            ?>

        </p>
    </div>

    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="tile">
                <form action="{{ url('/save-sub-category') }}" method="POST" role="form" enctype="multipart/form-data">
                    @csrf
                        <div class="tile-body">
                        <div class="form-group">
                        <label for="parent">Parent Category<span class="m-l-5 text-danger"> *</label>
                        <select id="selectError3" class="form-control custom-select mt-15" name="category_id" required>
                            <option disabled selected value> Select Category </option>
                                <?php
                                $all_published_category = DB::table('category')
                                                            ->get();
                                foreach($all_published_category as $v_category){ ?>
                                <option value="{{ $v_category->category_id }}">{{ $v_category->category_name }}</option>

                                <?php } ?>
                        </select>
                        </div>
                    </div>

                        <div class="form-group">
                            <label class="control-label" for="name">Sub Category Name <span class="m-l-5 text-danger"> *</span></label>
                            <input class="form-control" type="text" name="sub_category_name" required>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save Category</button>
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection