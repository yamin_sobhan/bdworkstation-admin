@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>All Sub Category</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Parent Category </th>
                                <th>Sub Category Name</th>
                                
                            </tr>
                        </thead>
                        @foreach ($all_sub_category_info as $v_sub_category)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_sub_category->sub_category_id}}</td>
                                        <td>{{ $v_sub_category->category_name }}</td>
                                        <td>{{ $v_sub_category->sub_category_name }}</td>

                                        
                                    </tr>
                            
                        </tbody>
                        @endforeach
                    </table>
                    {{ $all_sub_category_info->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

