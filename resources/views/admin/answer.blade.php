@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Answers</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>Question ID</th>
                                <th>Answer</th>
                                
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($answer_info as $v_answer)
                        @if ($v_answer->is_active == 1)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_answer->id }}</td>
                                        <td>{{ $v_answer->question_id }}</td>
                                        <td>{{ $v_answer->answer }}</td>
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/deactivate_answer/'.$v_answer->id) }}" class="btn btn-sm btn-success" id="verified"><i class="fa fa-check-circle"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $answer_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

