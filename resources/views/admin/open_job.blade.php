@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Open Jobs</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>User First Name </th>
                                <th>Category Name </th>
                                <th>Job Title</th>
                                <th>Description</th>
                                <th>Address</th>
                                <th>Area</th>
                                <th>Job Time</th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($open_job_info as $v_open_job)
                        @if ($v_open_job->is_done == 0 && $v_open_job->in_progress ==0 && $v_open_job->is_canceled == 0)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_open_job->job_id }}</td>
                                        <td>{{ $v_open_job->first_name }}</td>
                                        <td>{{ $v_open_job->category_name }}</td>
                                        <td>{{ $v_open_job->job_title }}</td>
                                        <td>{{ $v_open_job->job_description }}</td>
                                        <td>{{ $v_open_job->job_address }}</td>
                                        <td>{{ $v_open_job->area_name }}</td>
                                        <td>{{ $v_open_job->job_time }}</td>
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/cancel_job/'.$v_open_job->job_id) }}" class="btn btn-sm btn-danger" id="verified">Cancel</a>
                                            </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $open_job_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

