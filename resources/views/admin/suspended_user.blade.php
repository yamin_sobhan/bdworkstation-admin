@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Suspended User</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>First Name </th>
                                <th>Last Name </th>
                                <th>Phone Number</th>
                                <th>Address</th>
                                <th>Area</th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($suspended_user_info as $v_suspended_user)
                        
                        <tbody>
                                    <tr>
                                        <td>{{ $v_suspended_user->user_id }}</td>
                                        <td>{{ $v_suspended_user->first_name }}</td>
                                        <td>{{ $v_suspended_user->last_name }}</td>
                                        <td>{{ $v_suspended_user->phone_number }}</td>
                                        <td>{{ $v_suspended_user->address }}</td>
                                        <td>{{ $v_suspended_user->area_name }}</td>
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/profile_live/'.$v_suspended_user->user_id) }}" class="btn btn-sm btn-success" id="verified">Active</a>
                                            </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        
                        @endforeach
                    </table>
                    {{ $suspended_user_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

