@extends('admin_layout')

@section('admin_content')

<div class="app-title">
        <div>
            <h1><i class="fa fa-tags"></i>Active User</h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th>First Name </th>
                                <th>Last Name </th>
                                <th>Phone Number</th>
                                <th>Address</th>
                                <th>Area</th>
                                <th>File_type</th>
                                <th>Front</th>
                                <th>Back</th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                                <th style="width:100px; min-width:100px;" class="text-center text-danger"><i class="fa fa-bolt"> </i></th>
                            </tr>
                        </thead>
                        @foreach ($active_user_info as $v_active_user)
                        @if ($v_active_user->is_verified == 1 && $v_active_user->is_activated == 0 && $v_active_user->is_deleted == 0)
                        <tbody>
                                    <tr>
                                        <td>{{ $v_active_user->user_id }}</td>
                                        <td>{{ $v_active_user->first_name }}</td>
                                        <td>{{ $v_active_user->last_name }}</td>
                                        <td>{{ $v_active_user->phone_number }}</td>
                                        <td>{{ $v_active_user->address }}</td>
                                        <td>{{ $v_active_user->area_name }}</td>
                                        <td>{{ $v_active_user->file_type }}</td>
                            
                                        <td><a href="{{ URL::to('images/user/'.$v_active_user->verification_file_front) }}" target="_blank">Front Image</a></td>
                                        <td><a href="{{ URL::to('images/user/'.$v_active_user->verification_file_back) }}" target="_blank">Back Image</a></td>    
                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <a href="{{ URL::to('/delete_user/'.$v_active_user->user_id) }}" class="btn btn-sm btn-danger" id="verified">Delete</a>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                            <button class="btn btn-danger" type="button">Suspend</button>
                                            <div class="btn-group" role="group">
                                                 <button class="btn btn-danger dropdown-toggle" id="btnGroupDrop4" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                             <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{ URL::to('/user-suspend3/'.$v_active_user->user_id) }}">3 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/user-suspend5/'.$v_active_user->user_id) }}">5 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/user-suspend8/'.$v_active_user->user_id) }}">8 Days</a>
                                                                                            <a class="dropdown-item" href="{{ URL::to('/user-suspend10/'.$v_active_user->user_id) }}">10 Days</a>
                                             </div>
                                          </div>
                                         </div>
                                        </td>
                                    </tr>
                            
                        </tbody>
                        @endif
                        @endforeach
                    </table>
                    {{ $active_user_info->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

