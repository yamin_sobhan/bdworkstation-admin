<!DOCTYPE html>
<html lang="en">
<head>
    <title>WorkStation - Admin</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/main.css') }}" />
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/font-awesome/css/font-awesome.min.css') }}"/> -->

    <script src="https://use.fontawesome.com/fd21993f67.js"></script>

</head>
<body class="app sidebar-mini rtl">

    <!-- start: Header -->
<header class="app-header">
    <a class="app-header__logo" href="{{ url('/dashboard') }}">{{ config('app.name') }}</a>
    <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <ul class="app-nav">
        
        
        <!-- User Menu-->
        <li class="dropdown">
            <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li>
                    <a class="dropdown-item" href="{{ url('/logout') }}"><i class="fa fa-sign-out fa-lg"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>
</header>

<!-- start: Header -->



<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <div>
            <p class="app-sidebar__user-name">Admin</p>
        </div>
    </div>
    <ul class="app-menu">
        <li>
        <a class="app-menu__item active" href="{{ url('/dashboard') }}"><i class="app-menu__icon fa fa-dashboard"></i>
                <span class="app-menu__label">Dashboard</span>
            </a>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">User</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
               <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/new_user') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">New User</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/requested_user') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Requested User</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/active_user') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Active User</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/deleted_user') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Deleted User</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/suspended_user') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Suspended User</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Worker</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
            <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/new_worker') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">New Worker</span>
                    </a>
                </li>

                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/requested_worker') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Requested Worker</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/active_worker') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Active Worker</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/deleted_worker') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Deleted Worker</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/suspended_worker') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Suspended Worker</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Job</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/complete_job') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Complete Job</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/running_job') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Running Job</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{ URL::to('/open_job') }}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Open Jobs</span>
                    </a>
                </li>
            </ul>
        </li>


        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Report</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
            <li>
            <a class="app-menu__item"
                href="{{URL::to('/user_report')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">User Report</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item"
                href="{{URL::to('/worker_report')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Worker Report</span>
            </a>
        </li>
            </ul>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Transactions</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
            <li>
            <a class="app-menu__item"
                href="{{URL::to('/sms_transactions')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">SMS Transactions</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item"
                href="{{URL::to('/promotion_transactions')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Promotion Transactions</span>
            </a>
        </li>
            </ul>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">SMS Packages</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
            <li>
            <a class="app-menu__item"
                href="{{URL::to('/all_sms_package')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">All SMS Packages </span>
            </a>
        </li>
        <li>
            <a class="app-menu__item"
                href="{{URL::to('/add-sms-package')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Add SMS Packages </span>
            </a>
        </li>
            </ul>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Promotion Packages</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
            <li>
            <a class="app-menu__item"
                href="{{URL::to('/all_promotion_package')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">All Promotion Packages </span>
            </a>
        </li>
        <li>
            <a class="app-menu__item"
                href="{{URL::to('/add-promotion-package')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Add Promotion Packages </span>
            </a>
        </li>
            </ul>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Category</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
            <li>
            <a class="app-menu__item"
                href="{{URL::to('/all-category')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">All Categories</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item"
                href="{{URL::to('/add-category')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Add Categories</span>
            </a>
        </li>
            </ul>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Sub Category</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">

                <li>
                    <a class="app-menu__item"
                        href="{{URL::to('/add-sub-category')}}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Add Sub Categories</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{URL::to('/all-sub-category')}}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">All Sub Categories</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Area</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a class="app-menu__item"
                        href="{{URL::to('/add-area')}}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">Add Area</span>
                    </a>
                </li>
                <li>
                    <a class="app-menu__item"
                        href="{{URL::to('/all-area')}}">
                        <i class="app-menu__icon fa fa-tags"></i>
                        <span class="app-menu__label">All Area</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="treeview">
            <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
                <span class="app-menu__label">Services</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
            <ul class="treeview-menu">

        <li>
            <a class="app-menu__item"
                href="{{URL::to('/add-service')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Add Service</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item"
                href="{{URL::to('/all-service')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">All Service</span>
            </a>
        </li>
        </ul>
        </li>
        <li>
            <a class="app-menu__item"
                href="{{URL::to('/question')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Questions</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item"
                href="{{URL::to('/answer')}}">
                <i class="app-menu__icon fa fa-tags"></i>
                <span class="app-menu__label">Answers</span>
            </a>
        </li>
    </ul>
</aside>

    <main class="app-content" id="app">
        @yield('admin_content')
    </main>
    <script src="{{ asset('backend/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('backend/js/popper.min.js') }}"></script>
    <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/main.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/pace.min.js') }}"></script>
    @stack('scripts')
</body>
</html>