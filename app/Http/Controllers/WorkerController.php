<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use Carbon\Carbon;
use Session;
session_start();

class WorkerController extends Controller
{

    public function requested_worker()
    {
        $this->AdminAuthCheck();

        $requested_worker_info = DB::table('worker_table')
            ->join('worker_verification_file', 'worker_verification_file.worker_id', '=', 'worker_table.worker_id')
            ->join('area_table', 'area_table.area_id', '=', 'worker_table.area')
            ->join('category', 'category.category_id', '=', 'worker_table.category_id')
            ->select('worker_table.*', 'worker_verification_file.file_type', 
                     'worker_verification_file.verification_file_front', 
                     'worker_verification_file.verification_file_back', 'category.category_name', 'area_table.area_name')
            ->paginate(10);

        $manage_worker = view('admin.requested_worker')->with('requested_worker_info', $requested_worker_info);

        return view('admin_layout')->with('admin.requested_worker', $manage_worker);
    }

    public function new_worker()
    {
        $this->AdminAuthCheck();

        $new_worker_info = DB::table('worker_table')
            ->join('area_table', 'area_table.area_id', '=', 'worker_table.area')
            ->select('worker_table.*', 'area_table.area_name')
            ->WhereNull('profile_live_date')
            ->paginate(10);
            
        $new_worker = view('admin.new_worker')->with('new_worker_info', $new_worker_info);

        return view('admin_layout')->with('admin.new_worker', $new_worker);
    }

    public function active_worker()
    {
        $this->AdminAuthCheck();

        $active_worker_info = DB::table('worker_table')
                ->join('worker_verification_file', 'worker_verification_file.worker_id', '=', 'worker_table.worker_id')
                ->join('area_table', 'area_table.area_id', '=', 'worker_table.area')
                ->select('worker_table.*', 'area_table.area_name', 'worker_verification_file.file_type', 
                'worker_verification_file.verification_file_front', 
                'worker_verification_file.verification_file_back')
                ->WhereNull('profile_live_date')
                ->paginate(10);

        $manage_active_worker = view('admin.active_worker')->with('active_worker_info', $active_worker_info);

        return view('admin_layout')->with('admin.active_worker', $manage_active_worker);
    }

    public function deleted_worker()
    {
        $this->AdminAuthCheck();

        $deleted_worker_info = DB::table('worker_table')
                ->join('area_table', 'area_table.area_id', '=', 'worker_table.area')
                ->select('worker_table.*', 'area_table.area_name')
                ->paginate(10);

        $manage_deleted_worker = view('admin.deleted_worker')->with('deleted_worker_info', $deleted_worker_info);

        return view('admin_layout')->with('admin.deleted_worker', $manage_deleted_worker);
    }

    public function suspended_worker()
    {
        $this->AdminAuthCheck();

        $suspended_worker_info = DB::table('worker_table')
                ->join('area_table', 'area_table.area_id', '=', 'worker_table.area')
                ->select('worker_table.*', 'area_table.area_name')
                ->whereNotNull('profile_live_date')
                ->paginate(10);

        $manage_suspended_worker = view('admin.suspended_worker')->with('suspended_worker_info', $suspended_worker_info);

        return view('admin_layout')->with('admin.suspended_worker', $manage_suspended_worker);
    }

    public function worker_profile_live($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>NULL]);

        return redirect('/suspended_worker');
    }


    public function verified_worker($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['is_verified'=>1]);

        return redirect('/new_worker');
    }

    public function delete_worker($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['is_deleted'=>1]);

        return redirect('/active_worker');
    }

    public function delete_new_worker($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['is_deleted'=>1]);

        return redirect('/new_worker');
    }

    public function activate_worker($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['is_deleted'=>0]);

        return redirect('/deleted_worker');
    }

    public function worker_suspend3($worker_id)
    {
        
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(3)]);

        return redirect('/active_worker');
    }

    public function worker_suspend5($worker_id)
    {
        
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(5)]);

        return redirect('/active_worker');
    }

    public function worker_suspend8($worker_id)
    {
        
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(8)]);

        return redirect('/active_worker');
    }

    public function worker_suspend10($worker_id)
    {
        
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(10)]);

        return redirect('/active_worker');
    }

    public function new_worker_suspend3($worker_id)
    {
        
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(3)]);

        return redirect('/new_worker');
    }

    public function new_worker_suspend5($worker_id)
    {
        
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(5)]);

        return redirect('/new_worker');
    }

    public function new_worker_suspend8($worker_id)
    {
        
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(8)]);

        return redirect('/new_worker');
    }

    public function new_worker_suspend10($worker_id)
    {
        
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(10)]);

        return redirect('/new_worker');
    }

    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
