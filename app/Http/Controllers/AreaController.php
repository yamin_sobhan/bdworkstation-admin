<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use Session;
session_start();

class AreaController extends Controller
{
    public function index()
    {
        $this->AdminAuthCheck();
        return view('admin.add_area');
    }


    public function all_area()
    {
        $this->AdminAuthCheck();

        $all_area_info = DB::table('area_table')
            ->paginate(10);

        $manage_area = view('admin.all_area')
            ->with('all_area_info', $all_area_info);

        return view('admin_layout')
            ->with('admin.all_area', $manage_area);
    }

    public function save_area(Request $request)
    {
        $data=array();
        $data['area_id'] = $request->area_id;
        $data['area_name'] = $request->area_name;

        DB::table('area_table')->insert($data);
        Session::put('message', 'Area Added Successfully');
        return Redirect::to('/add-area');
    }

    public function delete_area($area_id)
    {
        DB::table('area_table')
            ->where('area_id', $area_id)
            ->delete();
        Session::get('message', 'Deleted Successfully');
        return Redirect::to('/all-area');

    }




    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
