<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use Session;
session_start();

class SubCategoryController extends Controller
{
    public function index()
    {
        $this->AdminAuthCheck();
        return view('admin.add_sub_category');
    }

    public function all_sub_category()
    {
        $this->AdminAuthCheck();

        $all_sub_category_info = DB::table('sub_category_table')
                    ->join('category', 'sub_category_table.category_id', '=', 'category.category_id')
                    ->select('sub_category_table.*', 'category.category_name')
                    ->paginate(10);

        $manage_sub_category = view('admin.all_sub_category')->with('all_sub_category_info', $all_sub_category_info);

        return view('admin_layout')->with('admin.all_sub_category', $manage_sub_category);
    }

    public function save_sub_category(Request $request)
    {
        $this->AdminAuthCheck();
        $data = array();
        $data['category_id'] = $request->category_id;
        $data['sub_category_name'] = $request->sub_category_name;

        DB::table('sub_category_table')->insert($data);
        Session::put('message', 'Sub Category Added Successfully');

        return Redirect::to('/add-sub-category');
    }

    public function delete_sub_category($sub_category_id)
    {
        $this->AdminAuthCheck();
        DB::table('sub_category_table')
            ->where('sub_category_id', $sub_category_id)
            ->delete();
        Session::get('message', 'Deleted Successfully');
        return Redirect::to('/all-sub-category');

    }


    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
