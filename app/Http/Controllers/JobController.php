<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use PDF;
use Session;
session_start();

class JobController extends Controller
{

    public function complete_job()
    {
        $this->AdminAuthCheck();

        $complete_job_info = DB::table('job_table')
                ->join('job_status', 'job_status.job_id', '=', 'job_table.job_id')
                ->join('user_table', 'user_table.user_id', '=', 'job_table.user_id')
                ->join('category', 'category.category_id', '=', 'job_table.category_id')
                ->join('area_table', 'area_table.area_id', '=', 'job_table.job_area')
                ->select('job_table.*', 'job_status.is_done', 'job_status.is_canceled', 'area_table.area_name', 'job_status.in_progress', 'user_table.first_name', 'category.category_name')
                ->paginate(10);

        $manage_complete_job = view('admin.complete_job')->with('complete_job_info', $complete_job_info);

        return view('admin_layout')->with('admin.complete_job', $manage_complete_job);
        
    }

    public function running_job()
    {
        $this->AdminAuthCheck();

        $running_job_info = DB::table('job_table')
                ->join('job_status', 'job_status.job_id', '=', 'job_table.job_id')
                ->join('user_table', 'user_table.user_id', '=', 'job_table.user_id')
                ->join('category', 'category.category_id', '=', 'job_table.category_id')
                ->join('area_table', 'area_table.area_id', '=', 'job_table.job_area')
                ->select('job_table.*', 'job_status.is_done', 'job_status.is_canceled', 'area_table.area_name', 'job_status.in_progress', 'user_table.first_name', 'category.category_name')
                ->paginate(10);

        $manage_running_job = view('admin.running_job')->with('running_job_info', $running_job_info);

        return view('admin_layout')->with('admin.running_job', $manage_running_job);
    }

    public function open_job()
    {
        $this->AdminAuthCheck();

        $open_job_info = DB::table('job_table')
                ->join('job_status', 'job_status.job_id', '=', 'job_table.job_id')
                ->join('user_table', 'user_table.user_id', '=', 'job_table.user_id')
                ->join('category', 'category.category_id', '=', 'job_table.category_id')
                ->join('area_table', 'area_table.area_id', '=', 'job_table.job_area')
                ->select('job_table.*', 'job_status.is_done', 'job_status.is_canceled', 'area_table.area_name', 'job_status.in_progress', 'user_table.first_name', 'category.category_name')
                ->paginate(10);

        $manage_open_job = view('admin.open_job')->with('open_job_info', $open_job_info);

        return view('admin_layout')->with('admin.open_job', $manage_open_job);
    }

    public function cancel_job($job_id)
    {
        DB::table('job_status')
            ->where('job_id', $job_id)
            ->update(['is_canceled'=>1]);

        return redirect('/open_job');
    }


    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
