<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use Session;
session_start();

class CategoryController extends Controller
{
    
    public function countCat()
    {
        $this->AdminAuthCheck();
    $count = DB::table('category')->count();
    $countSub = DB::table('sub_category_table')->count();
    $countArea = DB::table('area_table')->count();
    $countService = DB::table('service_table')->count();
    $count_requested_user = DB::table('user_table')->join('user_verification_file','user_verification_file.user_id','=','user_table.user_id')->where(['is_verified'=>0])->count();
    $count_active_user = DB::table('user_table')->whereNull('profile_live_date')->where(['is_verified'=>1])->where(['is_activated'=>0])->where(['is_deleted'=>0])->count();
    $count_deleted_user = DB::table('user_table')->where(['is_deleted'=>1])->count();
    $count_requested_worker = DB::table('worker_table')->join('worker_verification_file', 'worker_verification_file.worker_id','=','worker_table.worker_id')->where(['is_verified'=>0])->count();
    $count_active_worker = DB::table('worker_table')->whereNull('profile_live_date')->where(['is_verified'=>1])->where(['is_activated'=>0])->where(['is_deleted'=>0])->count();
    $count_deleted_worker = DB::table('worker_table')->where(['is_deleted'=>1])->count();
    $count_suspended_user = DB::table('user_table')->whereNotNull('profile_live_date')->count();
    $count_suspended_worker = DB::table('worker_table')->whereNotNull('profile_live_date')->count();
    $count_completed_job = DB::table('job_status')->where(['is_done'=>1])->where(['in_progress'=>1])->where(['is_canceled'=>0])->count();
    $count_running_job = DB::table('job_status')->where(['is_done'=>0])->where(['in_progress'=>1])->where(['is_canceled'=>0])->count();
    $count_open_job = DB::table('job_status')->where(['is_done'=>0])->where(['in_progress'=>0])->where(['is_canceled'=>0])->count();
    $count_user_report = DB::table('user_report_table')->where(['action'=>0])->count();
    $count_worker_report = DB::table('worker_report_table')->where(['action'=>0])->count();
    $count_sms_transactions = DB::table('transactions')->where(['transaction_type'=>'SMS_CREDIT'])->where(['is_approved'=>1])->count();
    $count_promotion_transactions = DB::table('transactions')->where(['transaction_type'=>'PROMOTION_CREDIT'])->where(['is_approved'=>1])->count();
    $count_sms_packages = DB::table('sms_package')->count();
    $count_promotion_packages = DB::table('promotion_package')->count();
    return view('admin.dashboard', 
        compact('count', 'countSub', 'countArea', 
                'countService', 'count_requested_user', 
                'count_active_user', 'count_deleted_user',
                'count_requested_worker','count_user_report','count_worker_report', 
                'count_open_job','count_completed_job','count_running_job', 
                'count_active_worker', 'count_deleted_worker', 'count_suspended_user', 
                'count_suspended_worker', 'count_sms_transactions', 'count_promotion_transactions',
                'count_sms_packages', 'count_promotion_packages'));
    }

    public function index()
    {
        $this->AdminAuthCheck();
        return view('admin.add_category');
    }


    public function all_category()
    {
        $this->AdminAuthCheck();

        $all_category_info = DB::table('category')
            ->paginate(10);

        $manage_category = view('admin.all_category')
            ->with('all_category_info', $all_category_info);

        return view('admin_layout')
            ->with('admin.all_category', $manage_category);
    }

    public function save_category(Request $request)
    {
        $data=array();
        $data['category_id'] = $request->category_id;
        $data['category_name'] = $request->category_name;

        DB::table('category')->insert($data);
        Session::put('message', 'Category Added Successfully');
        return Redirect::to('/add-category');
    }

    public function delete_category($category_id)
    {
        DB::table('category')
            ->where('category_id', $category_id)
            ->delete();
        Session::get('message', 'Deleted Successfully');
        return Redirect::to('/all-category');

    }




    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
