<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use Session;
session_start();

class SMSPackageController extends Controller
{

    public function index()
    {
        $this->AdminAuthCheck();
        return view('admin.add_sms_packages');
    }

    public function save_sms_package(Request $request)
    {
        $data=array();
        $data['id'] = $request->id;
        $data['package_name'] = $request->package_name;
        $data['package_price'] = $request->package_price;
        $data['package_validation_in_month'] = $request->package_validation_in_month;

        DB::table('sms_package')->insert($data);
        Session::put('message', 'Pacakge successfully Added');
        return Redirect::to('/add-sms-package');
    }

    public function all_sms_package()
    {
        $this->AdminAuthCheck();

        $all_sms_package = DB::table('sms_package')
            ->paginate(10);

        $manage_sms_package = view('admin.all_sms_package')
            ->with('all_sms_package', $all_sms_package);

        return view('admin_layout')->with('admin.all_sms_package', $manage_sms_package);
    }

    public function delete_sms_package($id)
    {
        DB::table('sms_package')
            ->where('id', $id)
            ->delete();
        Session::get('message', 'Package Deleted');
        return Redirect::to('/all_sms_package');
    }

    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
