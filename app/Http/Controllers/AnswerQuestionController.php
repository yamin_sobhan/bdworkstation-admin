<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use Carbon\Carbon;
use App\Http\Requests;
use Session;
session_start();

class AnswerQuestionController extends Controller
{

    public function question()
    {
        $this->AdminAuthCheck();

        $question_info = DB::table('question_table')
            ->paginate(10);

        $manage_question = view('admin.question')->with('question_info', $question_info);

        return view('admin_layout')->with('admin.question', $manage_question);
    }

    public function answer()
    {
        $this->AdminAuthCheck();

        $answer_info = DB::table('answer_table')
            ->paginate(10);

        $manage_answer = view('admin.answer')->with('answer_info', $answer_info);

        return view('admin_layout')->with('admin.answer', $manage_answer);
    }

    public function deactivate_question($id)
    {
        DB::table('question_table')
            ->where('id', $id)
            ->update(['is_active'=>0]);

        return redirect('/question');
    }

    public function deactivate_answer($id)
    {
        DB::table('answer_table')
            ->where('id', $id)
            ->update(['is_active'=>0]);

        return redirect('/answer');
    }

    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
