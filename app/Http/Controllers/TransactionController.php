<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Http\Requests;
use Session;
session_start();

class TransactionController extends Controller
{

    public function sms_transactions()
    {
        $this->AdminAuthCheck();

        $sms_info = DB::table('transactions')
            ->join('worker_table', 'worker_table.worker_id', '=', 'transactions.worker_id')
            ->join('sms_package', 'sms_package.id', '=', 'transactions.package_id')
            ->select('transactions.*', 'worker_table.first_name', 'worker_table.last_name', 'sms_package.package_name', 'sms_package.package_price')
            ->paginate(10);

        
        
        $manage_sms = view('admin.sms_transactions')->with('sms_info', $sms_info);
        

        return view('admin_layout')->with('admin.sms_transactions', $manage_sms);
    }

    public function promotion_transactions()
    {
        $this->AdminAuthCheck();

        $promotion_info = DB::table('transactions')
            ->join('worker_table', 'worker_table.worker_id', '=', 'transactions.worker_id')
            ->join('promotion_package', 'promotion_package.id', '=', 'transactions.package_id')
            ->select('transactions.*', 'worker_table.first_name', 'worker_table.last_name', 'promotion_package.package_name', 'promotion_package.package_price')
            ->paginate(10);

        
        
        $manage_promotion = view('admin.promotion_transactions')->with('promotion_info', $promotion_info);
        

        return view('admin_layout')->with('admin.promotion_transactions', $manage_promotion);
    }

    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
