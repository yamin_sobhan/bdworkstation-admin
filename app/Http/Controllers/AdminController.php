<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Http\Requests;
use Session;
session_start();

class AdminController extends Controller
{
    public function index()
    {
    	return view('admin_login');
    }

    public function dashboard(Request $request)
    {
        
        $admin_username = $request->admin_username;
        $admin_password = md5($request->admin_password);

        $result = DB::table('admin_table')
            ->where('admin_username', $admin_username)
            ->where('admin_password', $admin_password)
            ->first();

        if ($result) {
            Session::put('admin_name', $result->admin_name);
            Session::put('admin_id', $result->admin_id);

            return Redirect::to('/dashboard');
        } else {
            Session::put('message', 'Username or password invalid');

            return Redirect::to('/');
        }

    }

    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
