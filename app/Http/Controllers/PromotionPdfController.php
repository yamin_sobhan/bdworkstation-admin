<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use PDF;
use Session;
session_start();

class PromotionPdfController extends Controller
{
    public function index($transaction_id)
    {
        $promotion_data = $this->get_promotion_data($transaction_id);
        return view('admin.promotion_pdf')->with('promotion_data', $promotion_data);
    }   

    function get_promotion_data($transaction_id)
    {
        $promotion_data = DB::table('transactions')
            ->where('transaction_id', $transaction_id)
            ->join('worker_table', 'worker_table.worker_id', '=', 'transactions.worker_id')
            ->join('promotion_package', 'promotion_package.id', '=', 'transactions.package_id')
            ->select('transactions.*', 'worker_table.first_name', 'worker_table.last_name', 'promotion_package.package_name', 'promotion_package.package_price')
            ->get();

        return $promotion_data;
    }

    function pdf($transaction_id)
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->convert_promotion_data_to_html($transaction_id));

        return $pdf->stream();
    }

    function convert_promotion_data_to_html($transaction_id)
    {
        $promotion_data = $this->get_promotion_data($transaction_id);
        foreach($promotion_data as $promotion){
        $output = $output = '
        <style>
    #invoice{
  padding: 30px;
}

.invoice {
  position: relative;
  background-color: #FFF;
  min-height: 680px;
  padding: 15px
}

.invoice header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #3989c6
}

.invoice .company-details {
  text-align: right
}

.invoice .company-details .name {
  margin-top: 0;
  margin-bottom: 0
}

.invoice .contacts {
  margin-bottom: 20px
}

.invoice .invoice-to {
  text-align: left
}

.invoice .invoice-to .to {
  margin-top: 0;
  margin-bottom: 0
}

.invoice .invoice-details {
  text-align: right
}

.invoice .invoice-details .invoice-id {
  margin-top: 0;
  color: #3989c6
}

.invoice main {
  padding-bottom: 50px
}

.invoice main .thanks {
  margin-top: -100px;
  font-size: 2em;
  margin-bottom: 50px
}

.invoice main .notices {
  padding-left: 6px;
  border-left: 6px solid #3989c6
}

.invoice main .notices .notice {
  font-size: 1.2em
}

.invoice table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px
}

.invoice table td,.invoice table th {
  padding: 15px;
  background: #eee;
  border-bottom: 1px solid #fff
}

.invoice table th {
  white-space: nowrap;
  font-weight: 400;
  font-size: 16px
}

.invoice table td h3 {
  margin: 0;
  font-weight: 400;
  color: #3989c6;
  font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
  text-align: right;
  font-size: 1.2em
}

.invoice table .no {
  color: #fff;
  font-size: 1.6em;
  background: #3989c6
}

.invoice table .unit {
  background: #ddd
}

.invoice table .total {
  background: #3989c6;
  color: #fff
}

.invoice table tbody tr:last-child td {
  border: none
}

.invoice table tfoot td {
  background: 0 0;
  border-bottom: none;
  white-space: nowrap;
  text-align: right;
  padding: 10px 20px;
  font-size: 1.2em;
  border-top: 1px solid #aaa
}

.invoice table tfoot tr:first-child td {
  border-top: none
}

.invoice table tfoot tr:last-child td {
  color: #3989c6;
  font-size: 1.4em;
  border-top: 1px solid #3989c6
}

.invoice table tfoot tr td:first-child {
  border: none
}

.invoice footer {
  width: 100%;
  text-align: center;
  color: #777;
  border-top: 1px solid #aaa;
  padding: 8px 0
}

@media print {
  .invoice {
      font-size: 11px!important;
      overflow: hidden!important
  }

  .invoice footer {
      position: absolute;
      bottom: 10px;
      page-break-after: always
  }

  .invoice>div:last-child {
      page-break-before: always
  }
}
  </style>
        
        <h1 align="center">Work Station</h1>
        <div id="invoice">
        <div class="invoice overflow-auto">
            <div style="min-width: 600px">
                <header>
                    <div class="row">
                        
                        <div class="col company-details">
                            <h2 class="name">
                                WorkStation-Admin
                            </h2>
                            <div>High Level Road, Wasa Circle</div>
                            <div>+88 01859217218</div>
                            <div>bdworkstation@gmail.com</div>
                        </div>
                    </div>
          </header>
          <main>
              <div class="row contacts">
                  <div class="col invoice-to">
                      <div class="text-gray-light">INVOICE TO:</div>
                      <h2 class="to">'.$promotion->first_name.' '.$promotion->last_name.'</h2>
                      
                      <div class="date">Date of Transaction: '.$promotion->transaction_time.'</div>
                  </div>
                  <div class="col invoice-details">
                      <h4 class="invoice-id">INVOICE: '.$promotion->transaction_id.'</h4>
                      
                      
                  </div>
              </div>
              <table border="0" cellspacing="0" cellpadding="0">
                  <thead>
                      <tr>
                          
                          <th class="text-left">Package Name</th>
                          <th class="text-right">Payment Method</th>
                          <th class="text-right">Transaction Type</th>
                          <th class="text-right">Package Price</th>
                      </tr>
                  </thead>
                  <tbody>
                      
                      <tr>
                          
                          <td class="text-left"><h3>'.$promotion->package_name.'</h3></td>
                          <td class="unit">'.$promotion->payment_method.'</td>
                          <td class="unit">'.$promotion->transaction_type.'</td>
                          <td class="total">'.$promotion->package_price.'</td>
                      </tr>
                      
                  </tbody>
                  
              </table>
              <div class="thanks" style="margin-top:20px;">Thank you!</div>
              
          </main>
          <footer>
              Invoice was created on a computer and is valid without the signature and seal.
          </footer>
      </div>
      
      <div></div>
  </div>
  </div>
      ';
     
    }
     return $output;
    }
}
