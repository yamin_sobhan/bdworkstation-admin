<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use Carbon\Carbon;
use App\Http\Requests;
use Session;
session_start();

class UserController extends Controller
{
    public function requested_user()
    {
        $this->AdminAuthCheck();

        $requested_user_info = DB::table('user_table')
            ->join('user_verification_file', 'user_verification_file.user_id', '=', 'user_table.user_id')
            ->join('area_table', 'area_table.area_id', '=', 'user_table.area')
            ->select('user_table.*', 'user_verification_file.file_type', 'user_verification_file.verification_file_front',  'user_verification_file.verification_file_back', 'area_table.area_name')
            ->paginate(10);

        $manage_user = view('admin.requested_user')->with('requested_user_info', $requested_user_info);

        return view('admin_layout')->with('admin.requested_user', $manage_user);
    }

    public function new_user()
    {
        $this->AdminAuthCheck();

        $new_user_info = DB::table('user_table')
            ->join('area_table', 'area_table.area_id', '=', 'user_table.area')
            ->select('user_table.*', 'area_table.area_name')
            ->WhereNull('profile_live_date')
            ->paginate(10);
            
        $new_user = view('admin.new_user')->with('new_user_info', $new_user_info);

        return view('admin_layout')->with('admin.new_user', $new_user);
    }

    public function active_user()
    {
        $this->AdminAuthCheck();

        $active_user_info = DB::table('user_table')
                ->join('user_verification_file', 'user_verification_file.user_id', '=', 'user_table.user_id')
                ->join('area_table', 'area_table.area_id', '=', 'user_table.area')
                ->select('user_table.*', 'area_table.area_name', 'user_verification_file.file_type', 'user_verification_file.verification_file_front',  'user_verification_file.verification_file_back')
                ->WhereNull('profile_live_date')
                ->paginate(10);

        $manage_active_user = view('admin.active_user')->with('active_user_info', $active_user_info);

        return view('admin_layout')->with('admin.active_user', $manage_active_user);
    }

    public function deleted_user()
    {
        $this->AdminAuthCheck();

        $deleted_user_info = DB::table('user_table')
                ->join('area_table', 'area_table.area_id', '=', 'user_table.area')
                ->select('user_table.*', 'area_table.area_name')
                ->paginate(10);

        $manage_deleted_user = view('admin.deleted_user')->with('deleted_user_info', $deleted_user_info);

        return view('admin_layout')->with('admin.deleted_user', $manage_deleted_user);
    }

    public function suspended_user()
    {
        $this->AdminAuthCheck();

        $suspended_user_info = DB::table('user_table')
                ->join('area_table', 'area_table.area_id', '=', 'user_table.area')
                ->select('user_table.*', 'area_table.area_name')
                ->whereNotNull('profile_live_date')
                ->paginate(10);

        $manage_suspended_user = view('admin.suspended_user')->with('suspended_user_info', $suspended_user_info);

        return view('admin_layout')->with('admin.suspended_user', $manage_suspended_user);
    }

    public function profile_live($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->update(['profile_live_date'=>NULL]);

        return redirect('/suspended_user');
    }

    public function user_suspend3($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(3)]);

        return redirect('/active_user');
    }

    public function user_suspend5($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(5)]);

        return redirect('/active_user');
    }

    public function user_suspend8($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(8)]);

        return redirect('/active_user');
    }

    public function user_suspend10($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(10)]);

        return redirect('/active_user');
    }

    public function new_user_suspend3($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(3)]);

        return redirect('/new_user');
    }

    public function new_user_suspend5($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(5)]);

        return redirect('/new_user');
    }

    public function new_user_suspend8($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(8)]);

        return redirect('/new_user');
    }

    public function new_user_suspend10($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(10)]);

        return redirect('/new_user');
    }

    public function verified_user($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['is_verified'=>1]);

        return redirect('/new_user');
    }

    public function delete_user($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['is_deleted'=>1]);

        return redirect('/active_user');
    }

    public function delete_new_user($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['is_deleted'=>1]);

        return redirect('/new_user');
    }

    public function activate_user($user_id)
    {
        $this->AdminAuthCheck();
        
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['is_deleted'=>0]);

        return redirect('/deleted_user');
    }


    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}