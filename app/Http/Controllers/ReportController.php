<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use PDF;
use Carbon\Carbon;
use Session;
session_start();

class ReportController extends Controller
{

    public function user_report()
    {
        $this->AdminAuthCheck();

        $user_report_info = DB::table('user_report_table')
            ->join('user_table', 'user_report_table.user_id', '=', 'user_table.user_id')
            ->join('worker_table', 'user_report_table.from_worker', '=', 'worker_table.worker_id')
            ->select('user_report_table.*', 'user_table.first_name', 'user_table.last_name', 'worker_table.worker_id')
            ->paginate(10);

        $manage_user_report = view('admin.user_report')->with('user_report_info', $user_report_info);

        return view('admin_layout')->with('admin.user_report', $manage_user_report);
    }

    public function user_report_suspend3($user_id)
    {
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(3)]);
        DB::table('user_report_table')
            ->where('user_id', $user_id)
            ->update(['action'=>1]);

        return redirect('/user_report');

    }

    public function user_report_suspend5($user_id)
    {
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(5)]);
        DB::table('user_report_table')
            ->where('user_id', $user_id)
            ->update(['action'=>1]);

        return redirect('/user_report');

    }

    public function user_report_suspend8($user_id)
    {
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(8)]);
        DB::table('user_report_table')
            ->where('user_id', $user_id)
            ->update(['action'=>1]);

        return redirect('/user_report');

    }

    public function user_report_suspend10($user_id)
    {
        DB::table('user_table')
            ->where('user_id', $user_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(10)]);
        DB::table('user_report_table')
            ->where('user_id', $user_id)
            ->update(['action'=>1]);

        return redirect('/user_report');

    }

    public function worker_report()
    {
        $this->AdminAuthCheck();

        $worker_report_info = DB::table('worker_report_table')
            ->join('user_table', 'worker_report_table.from_user', '=', 'user_table.user_id')
            ->join('worker_table', 'worker_report_table.worker_id', '=', 'worker_table.worker_id')
            ->select('worker_report_table.*', 'worker_table.first_name', 'worker_table.last_name', 'user_table.user_id')
            ->paginate(10);

        $manage_worker_report = view('admin.worker_report')->with('worker_report_info', $worker_report_info);

        return view('admin_layout')->with('admin.worker_report', $manage_worker_report);
    }

    public function worker_report_suspend3($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(3)]);
        DB::table('worker_report_table')
            ->where('worker_id', $worker_id)
            ->update(['action'=>1]);

        return redirect('/worker_report');

    }

    public function worker_report_suspend5($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(5)]);
        DB::table('worker_report_table')
            ->where('worker_id', $worker_id)
            ->update(['action'=>1]);

        return redirect('/worker_report');

    }

    public function worker_report_suspend8($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(8)]);
        DB::table('worker_report_table')
            ->where('worker_id', $worker_id)
            ->update(['action'=>1]);

        return redirect('/worker_report');

    }

    public function worker_report_suspend10($worker_id)
    {
        DB::table('worker_table')
            ->where('worker_id', $worker_id)
            ->update(['profile_live_date'=>Carbon::now()->addDays(10)]);
        DB::table('worker_report_table')
            ->where('worker_id', $worker_id)
            ->update(['action'=>1]);

        return redirect('/worker_report');

    }

    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
