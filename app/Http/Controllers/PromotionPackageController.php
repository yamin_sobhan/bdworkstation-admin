<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use Session;
session_start();

class PromotionPackageController extends Controller
{

    public function index()
    {
        $this->AdminAuthCheck();
        return view('admin.add_promotion_package');
    }

    public function all_promotion_package()
    {
        $this->AdminAuthCheck();

        $all_promotion_package = DB::table('promotion_package')
            ->paginate(10);

        $manage_promotion_package = view('admin.all_promotion_package')
            ->with('all_promotion_package', $all_promotion_package);

        return view('admin_layout')->with('admin.all_promotion_package', $manage_promotion_package);
    }

    public function save_promotion_package(Request $request)
    {
        $data=array();
        $data['id'] = $request->id;
        $data['package_name'] = $request->package_name;
        $data['package_price'] = $request->package_price;
        $data['package_validation_in_days'] = $request->package_validation_in_days;

        DB::table('promotion_package')->insert($data);
        Session::put('message', 'Pacakge successfully Added');
        return Redirect::to('/add-promotion-package');
    }

    public function delete_promotion_package($id)
    {
        DB::table('promotion_package')
            ->where('id', $id)
            ->delete();
        Session::get('message', 'Package Deleted');
        return Redirect::to('/all_promotion_package');
    }

    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}
}
