<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Illuminate\Support\Collection;
use DB;
use App\Http\Requests;
use Session;
session_start();

class ServiceController extends Controller
{
    public function index()
    {
        $this->AdminAuthCheck();
        return view('admin.add_service');
    }

    public function all_service()
    {
        $this->AdminAuthCheck();

        $all_service_info = DB::table('service_table')
            ->join('sub_category_table', 'service_table.sub_category_id', '=', 'sub_category_table.sub_category_id')
            ->select('service_table.*', 'sub_category_table.sub_category_name')
            ->paginate(10);

            $manage_service = view('admin.all_service')
            ->with('all_service_info', $all_service_info);

        return view('admin_layout')
            ->with('admin.all_service', $manage_service);
    }

    public function save_service(Request $request)
    {
        $data = array();
        $data['sub_category_id']=$request->sub_category_id;
        $data['service_name']=$request->service_name;

        DB::table('service_table')->insert($data);
        Session::put('message', 'Service Successfully added');
        return Redirect::to('/add-service');        

    }

    public function delete_service($service_id)
    {
        DB::table('service_table')
            ->where('service_id', $service_id)
            ->delete();
        Session::get('message', 'Deleted Successfully');
        return Redirect::to('/all-service');

    }

    public function AdminAuthCheck()
	{
		$admin_id=Session::get('admin_id');
		if($admin_id){
			return;
		}else {
			return Redirect::to('/')->send();
		}
	}

}
