<?php

Route::get('/', 'AdminController@index');

Route::post('/admin-dashboard', 'AdminController@dashboard');

Route::get('/dashboard', 'SuperAdminController@index');
Route::get('/logout', 'SuperAdminController@logout');


Route::get('/add-category', 'CategoryController@index');
Route::post('/save-category', 'CategoryController@save_category');
Route::get('/all-category', 'CategoryController@all_category');
Route::get('/delete_category/{category_id}', 'CategoryController@delete_category');
Route::get('/dashboard', 'CategoryController@countCat');

Route::get('/add-sub-category', 'SubCategoryController@index');
Route::post('/save-sub-category', 'SubCategoryController@save_sub_category');
Route::get('/all-sub-category', 'SubCategoryController@all_sub_category');
Route::get('/delete_sub_category/{sub_category_id}', 'SubCategoryController@delete_sub_category');

Route::get('/add-area', 'AreaController@index');
Route::post('/save-area', 'AreaController@save_area');
Route::get('/all-area', 'AreaController@all_area');
Route::get('/delete_area/{area_id}', 'AreaController@delete_area');

Route::get('/add-service', 'ServiceController@index');
Route::post('/save-service', 'ServiceController@save_service');
Route::get('/all-service', 'ServiceController@all_service');
Route::get('/delete_service/{service_id}', 'ServiceController@delete_service');

Route::get('/sms_transactions', 'TransactionController@sms_transactions');
Route::get('/promotion_transactions', 'TransactionController@promotion_transactions');


Route::get('/sms_pdf/{transaction_id}', 'SMSPdfController@index');
Route::get('/sms_pdf/sms_pdf/{transaction_id}', 'SMSPdfController@pdf');

Route::get('/promotion_pdf/{transaction_id}', 'PromotionPdfController@index');
Route::get('/promotion_pdf/promotion_pdf/{transaction_id}', 'PromotionPdfController@pdf');

Route::get('/all_sms_package', 'SMSPackageController@all_sms_package');
Route::get('/add-sms-package', 'SMSPackageController@index');
Route::post('/save_sms_package', 'SMSPackageController@save_sms_package');
Route::get('/delete_sms_package/{id}', 'SMSPackageController@delete_sms_package');

Route::get('/all_promotion_package', 'PromotionPackageController@all_promotion_package');
Route::get('/add-promotion-package', 'PromotionPackageController@index');
Route::post('/save_promotion_package', 'PromotionPackageController@save_promotion_package');
Route::get('/delete_promotion_package/{id}', 'PromotionPackageController@delete_promotion_package');

Route::get('/requested_user', 'UserController@requested_user');

Route::get('/verified_user/{user_id}', 'UserController@verified_user');
Route::get('/active_user', 'UserController@active_user');
Route::get('/new_user', 'UserController@new_user');
Route::get('/delete_new_user/{user_id}', 'UserController@delete_new_user');
Route::get('/delete_user/{user_id}', 'UserController@delete_user');
Route::get('/deleted_user', 'UserController@deleted_user');
Route::get('/activate_user/{user_id}', 'UserController@activate_user');
Route::get('/suspended_user', 'UserController@suspended_user');
Route::get('/profile_live/{user_id}', 'UserController@profile_live');
Route::get('/user-suspend3/{user_id}', 'UserController@user_suspend3');
Route::get('/user-suspend5/{user_id}', 'UserController@user_suspend5');
Route::get('/user-suspend8/{user_id}', 'UserController@user_suspend8');
Route::get('/user-suspend10/{user_id}', 'UserController@user_suspend10');
Route::get('/new-user-suspend3/{user_id}', 'UserController@new_user_suspend3');
Route::get('/new-user-suspend5/{user_id}', 'UserController@new_user_suspend5');
Route::get('/new-user-suspend8/{user_id}', 'UserController@new_user_suspend8');
Route::get('/new-user-suspend10/{user_id}', 'UserController@new_user_suspend10');

Route::get('/requested_worker', 'WorkerController@requested_worker');
Route::get('/new_worker', 'WorkerController@new_worker');
Route::get('/verified_worker/{worker_id}', 'WorkerController@verified_worker');
Route::get('/active_worker', 'WorkerController@active_worker');
Route::get('/deletenew_new_worker/{worker_id}', 'WorkerController@delete_new_worker');
Route::get('/delete_worker/{worker_id}', 'WorkerController@delete_worker');
Route::get('/deleted_worker', 'WorkerController@deleted_worker');
Route::get('/activate_worker/{worker_id}', 'WorkerController@activate_worker');
Route::get('/suspended_worker', 'WorkerController@suspended_worker');
Route::get('/worker_profile_live/{worker_id}', 'WorkerController@worker_profile_live');
Route::get('/worker-suspend3/{worker_id}', 'WorkerController@worker_suspend3');
Route::get('/worker-suspend5/{worker_id}', 'WorkerController@worker_suspend5');
Route::get('/worker-suspend8/{worker_id}', 'WorkerController@worker_suspend8');
Route::get('/worker-suspend10/{worker_id}', 'WorkerController@worker_suspend10');
Route::get('/new-worker-suspend3/{worker_id}', 'WorkerController@new_worker_suspend3');
Route::get('/new-worker-suspend5/{worker_id}', 'WorkerController@new_worker_suspend5');
Route::get('/new-worker-suspend8/{worker_id}', 'WorkerController@new_worker_suspend8');
Route::get('/new-worker-suspend10/{worker_id}', 'WorkerController@new_worker_suspend10');

Route::get('/complete_job', 'JobController@complete_job');
Route::get('/running_job', 'JobController@running_job');
Route::get('/open_job', 'JobController@open_job');
Route::get('/cancel_job/{job_id}', 'JobController@cancel_job');

Route::get('/pdf/{job_id}', 'PdfController@index');
Route::get('/pdf/pdf/{job_id}', 'PdfController@pdf');


Route::get('/user_report', 'ReportController@user_report');
Route::get('/user-report-suspend3/{user_id}', 'ReportController@user_report_suspend3');
Route::get('/user-report-suspend5/{user_id}', 'ReportController@user_report_suspend5');
Route::get('/user-report-suspend8/{user_id}', 'ReportController@user_report_suspend8');
Route::get('/user-report-suspend10/{user_id}', 'ReportController@user_report_suspend10');


Route::get('/worker_report', 'ReportController@worker_report');
Route::get('/worker-report-suspend3/{worker_id}', 'ReportController@worker_report_suspend3');
Route::get('/worker-report-suspend5/{worker_id}', 'ReportController@worker_report_suspend5');
Route::get('/worker-report-suspend8/{worker_id}', 'ReportController@worker_report_suspend8');
Route::get('/worker-report-suspend10/{worker_id}', 'ReportController@worker_report_suspend10');

Route::get('/question', 'AnswerQuestionController@question');
Route::get('/deactivate_question/{id}', 'AnswerQuestionController@deactivate_question');

Route::get('/answer', 'AnswerQuestionController@answer');
Route::get('/deactivate_answer/{id}', 'AnswerQuestionController@deactivate_answer');


