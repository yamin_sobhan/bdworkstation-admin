<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_table', function (Blueprint $table) {
            $table->increments('worker_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone_number');
            $table->string('address');
            $table->string('area');
            $table->string('image');
            $table->string('password');
            $table->integer('category_id');
            $table->string('sub_category_id');
            $table->integer('got_paid');
            $table->integer('is_promoted')->default(0);
            $table->integer('is_verified')->default(0);
            $table->integer('is_activated')->default(0);
            $table->dateTime('profile_live_date')->nullable();
            $table->integer('is_deleted')->default(0);
            $table->integer('is_sms_on')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_table');
    }
}
