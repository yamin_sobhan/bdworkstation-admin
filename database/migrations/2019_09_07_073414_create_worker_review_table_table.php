<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerReviewTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_review_table', function (Blueprint $table) {
            $table->increments('worker_review_id');
            $table->integer('job_id');
            $table->integer('worker_id');
            $table->integer('from_user_review');
            $table->string('review_description');
            $table->integer('from_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_review_table');
    }
}
