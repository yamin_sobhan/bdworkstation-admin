<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReportTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_report_table', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('report_type');
            $table->string('report_description');
            $table->integer('from_worker');
            $table->integer('action')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_report_table');
    }
}
