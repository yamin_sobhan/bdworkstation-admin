<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerNotificationTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_notification_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('not_type');
            $table->integer('for_worker');
            $table->integer('job_id');
            $table->integer('not_read')->default(0);
            $table->dateTime('not_time')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_notification_table');
    }
}
