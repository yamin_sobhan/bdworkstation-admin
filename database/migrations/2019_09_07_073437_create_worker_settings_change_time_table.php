<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerSettingsChangeTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_settings_change_time', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('worker_id');
            $table->integer('number_of_changes');
            $table->dateTime('last_change');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_settings_change_time');
    }
}
