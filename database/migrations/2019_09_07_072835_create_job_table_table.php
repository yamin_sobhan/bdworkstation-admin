<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_table', function (Blueprint $table) {
            $table->increments('job_id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->integer('service_id');
            $table->string('job_title');
            $table->string('job_description', 500);
            $table->string('job_address');
            $table->integer('job_area');
            $table->string('job_time');
            $table->integer('user_id');
            $table->dateTime('post_time')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('job_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_table');
    }
}
