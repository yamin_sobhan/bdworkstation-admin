<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('is_canceled')->default(0);
            $table->integer('in_progress')->default(0);
            $table->integer('is_started')->default(0);
            $table->integer('user_complete')->default(0);
            $table->integer('worker_complete')->default(0);
            $table->integer('is_done')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_status');
    }
}
