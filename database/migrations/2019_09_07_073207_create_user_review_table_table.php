<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReviewTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_review_table', function (Blueprint $table) {
            $table->increments('user_review_id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->integer('from_worker_review');
            $table->string('review_description');
            $table->integer('from_worker_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_review_table');
    }
}
