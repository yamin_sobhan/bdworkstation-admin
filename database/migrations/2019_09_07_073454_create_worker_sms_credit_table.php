<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerSmsCreditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_sms_credit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('worker_id');
            $table->integer('package_id');
            $table->dateTime('buy_date');
            $table->dateTime('end_date');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_sms_credit');
    }
}
