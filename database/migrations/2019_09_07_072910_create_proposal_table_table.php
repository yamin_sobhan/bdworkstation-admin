<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal_table', function (Blueprint $table) {
            $table->increments('proposal_id');
            $table->integer('job_id');
            $table->integer('worker_id');
            $table->integer('proposal_price');
            $table->dateTime('Time');
            $table->integer('user_cancel')->default(0);
            $table->integer('worker_cancel')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposal_table');
    }
}
