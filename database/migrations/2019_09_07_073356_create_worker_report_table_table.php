<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerReportTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_report_table', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('worker_id');
            $table->string('report_type');
            $table->string('report_description');
            $table->integer('from_user');
            $table->integer('action')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_report_table');
    }
}
