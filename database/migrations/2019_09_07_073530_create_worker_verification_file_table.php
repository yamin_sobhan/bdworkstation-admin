<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerVerificationFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_verification_file', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('worker_id');
            $table->string('file_type');
            $table->string('verification_file_front');
            $table->string('verification_file_back');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_verification_file');
    }
}
